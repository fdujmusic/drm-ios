# MP3 stream decryptor

Simple iOS app which demonstrates streaming of encrypted mp3 audio while decrypting data on the fly.
Seeking within audio resource is also supported.

To play different resource, one should replace default url in ``AppDelegate`` with a new one.

## Decryption

Encryption architecture proposed here is partial key encryption.
Each resource is encrypted using **AES-256** algorithm.
Therefore, to successfully play the resource some requirements should be met:

1. **Common key** shared between storage and client is a 256-bit key and must be hardcoded into the app itself.
2. **Local key** and **initialization vector** (IV) specific to requested resource are baked into the resource name during encryption phase.
3. **Resource name** should be constructed as following: ```resourcename_encryptedlocalkey_vectorprefix.mp3``` 

    * ```encryptedlocalkey``` 
    
    Specific 256-bit key encrypted with common key and IV , encoded as hex string.

    * ```vectorprefix```   
    
    First 12 bytes of specific initialization vector encoded as hex string. During the encryption phase IV is constructed
    as a 12 byte cryptographically secure random hex encoded string padded with 4 bytes of zeros, 
    making total of 16 bytes which is required vector length for AES-256. Therefore, client is only provided with 12 bytes
    of prefix, knowing it has to make it complete by adding zeros.

## Dependencies

Two major components used in app are:

1. [Embassy](https://github.com/envoy/Embassy) 

    Lightweight async http server. Acts as a proxy and keeps connections to both app and remote resource. 
    Its simple role is to read data from remote, decrypt received chunks and forward those to app.

2. [CommonCrypto library](http://www.manpagez.com/man/3/CC_crypto/)

    Common Crypto provides low-level C support for encryption and decryption. Way faster than Swift implementations. 
    Since processing speed is crucial in streaming application, this one is used rather than pure Swift.  
