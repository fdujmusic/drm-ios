//
//  String+CustomSchemes.swift
//  RotanaDecrypt
//
//  Created by Filip Dujmušić on 15/11/2017.
//  Copyright © 2017 Ingemark. All rights reserved.
//

import Foundation

extension URL {
    
    func changeBaseTo(_ host: Host) -> URL? {
        let newBase = host.fullPath
        let path = relativePath
        let baseURL = URL(string: newBase)
        return baseURL?.appendingPathComponent(path)
    }

    func getIVPrefix() -> String? {
        guard
            let lastUnderscoreIndex = lastPathComponent.range(of: "_", options: .backwards)?.lowerBound,
            let lastDotIndex = lastPathComponent.range(of: ".", options: .backwards)?.lowerBound
        else { return nil }
        
        let ivStartIndex = lastPathComponent.index(lastUnderscoreIndex, offsetBy: 1)
        return String(lastPathComponent[ivStartIndex..<lastDotIndex])
    }
    
    func getEncryptedLocalKey() -> String? {
        guard
            let lastUnderscoreIndex = lastPathComponent.range(of: "_", options: .backwards)?.lowerBound
        else { return nil }
        let partialFileName = lastPathComponent[..<lastUnderscoreIndex]
        
        guard
            let firstUnderscoreIndex = partialFileName.range(of: "_", options: .backwards)?.lowerBound
        else { return nil }
        let firstKeyIndex = partialFileName.index(firstUnderscoreIndex, offsetBy: 1)

        return String(partialFileName[firstKeyIndex...])
    }
    
}
