//
//  StreamingTask.swift
//  RotanaDecrypt
//
//  Created by Filip Dujmušić on 23/11/2017.
//  Copyright © 2017 Ingemark. All rights reserved.
//

import Foundation
import Embassy

class StreamingDataTask: NSObject {
    
    typealias SendBodyFunction = (Data) -> Void
    typealias StartResponseFunction = (String, [(String, String)]) -> Void
    
    var environment: [String : Any]
    var startResponse: StartResponseFunction
    var sendBody: SendBodyFunction
    var loop: EventLoop?
    
    var session: URLSession?
    var decryptor: Decryptor?

    init(environment: [String : Any], startResponse: @escaping StartResponseFunction, sendBody: @escaping SendBodyFunction, loop: EventLoop) {
        self.environment = environment
        self.startResponse = startResponse
        self.sendBody = sendBody
        self.loop = loop
        super.init()
    }
    
    func resume() {
        guard
            let request = ProxyUtilities.createRequest(from: environment),
            let url = request.url
        else { print("ERROR: Could not create request from environment parameters."); return }
        
        let infoRequest = NSMutableURLRequest(url: url)
        infoRequest.httpMethod = "HEAD"
        
        session = createSession()
        session?.dataTask(with: infoRequest as URLRequest) { (data, response, error) in
            guard
                let httpResponse = response as? HTTPURLResponse,
                let httpResponseHeaders = httpResponse.allHeaderFields as? [String : String],
                let contentSizeString = httpResponseHeaders[Constants.HTTP.length],
                let contentSize = Int(contentSizeString)
            else { return }
            
            self.loop?.call {
                self.startResponse(
                    "\(httpResponse.statusCode)",
                    httpResponseHeaders.map({ $0 })
                )
            }

            if let range = self.environment[Constants.ENV.range] as? String {
                let rangeStart = range.suffix(from: range.index(range.startIndex, offsetBy: 6))
                let components = rangeStart.components(separatedBy: "/")[0].components(separatedBy: "-")
                if let offset = Int(components[0]) {
                    self.decryptor = Decryptor(for: url, offset: offset, size: contentSize)
                }
            } else {
                self.decryptor = Decryptor(for: url, offset: 0, size: contentSize)
            }
            
            // start fetching and transcoding chunks
            self.session?.dataTask(with: request).resume()
        }.resume()

    }
    
    func cancel() {
        loop?.call {
            self.sendBody(Data())
        }
        session?.delegateQueue.cancelAllOperations()
        session?.invalidateAndCancel()
    }
    
    private func createSession() -> URLSession {
        let queue = OperationQueue()
        queue.maxConcurrentOperationCount = 1
        queue.qualityOfService = .background
        return URLSession(
            configuration: .default,
            delegate: self,
            delegateQueue: queue
        )
    }
    
}

extension StreamingDataTask: URLSessionDataDelegate {
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        guard
            let decryptor = self.decryptor,
            let decryptedData = decryptor.decrypt(data: data)
        else { print("ERROR: Could not decrypt data."); return }

        session.delegateQueue.addOperation {
            self.loop?.call {
                self.sendBody(decryptedData)
            }
        }
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        guard
            let decryptor = self.decryptor
        else { return }
        
        if let decodedLeftoverBytes = decryptor.finish() {
            session.delegateQueue.addOperation {
                self.loop?.call {
                    self.sendBody(decodedLeftoverBytes)
                }
            }
        }

        session.delegateQueue.addOperation {
            self.loop?.call {
                self.sendBody(Data())
            }
        }
    }
    
}
