//
//  ProxyUtilities.swift
//  RotanaDecrypt
//
//  Created by Filip Dujmušić on 23/11/2017.
//  Copyright © 2017 Ingemark. All rights reserved.
//

import Foundation
import Embassy

struct ProxyUtilities { private init() { }

    static func createRequest(from environment: [String : Any]) -> URLRequest? {
        guard
            let method = environment[Constants.ENV.method] as? String
        else { print ("ERROR: Invalid request. Missing method.");  return nil }
        
        guard
            let pathString = environment[Constants.ENV.path] as? String,
            let pathURL = URL(string: pathString),
            let newURL = pathURL.changeBaseTo(.s3)
        else { print ("ERROR: Invalid request url."); return nil }
        
        let urlRequest = NSMutableURLRequest(url: newURL)
        urlRequest.httpMethod = method
        
        return fillRequest(urlRequest, withHeaders: environment)
    }
    
    private static func fillRequest(_ request: NSMutableURLRequest, withHeaders environment: [String : Any]) -> URLRequest {
        if let acceptEncoding = environment[Constants.ENV.encoding] as? String {
            request.addValue(acceptEncoding, forHTTPHeaderField: Constants.HTTP.encoding)
        }
        
        if let byteRange = environment[Constants.ENV.range] as? String {
            request.addValue(byteRange, forHTTPHeaderField: Constants.HTTP.range)
        }
        
        if let connection = environment[Constants.ENV.connection] as? String {
            request.addValue(connection, forHTTPHeaderField: Constants.HTTP.connection)
        }
        
        if let acceptLanguage = environment[Constants.ENV.language] as? String {
            request.addValue(acceptLanguage, forHTTPHeaderField: Constants.HTTP.language)
        }
        
        if let accept = environment[Constants.ENV.accept] as? String {
            request.addValue(accept, forHTTPHeaderField: Constants.HTTP.accept)
        }
        if let playbackSessionID = environment[Constants.ENV.playback] as? String {
            request.addValue(playbackSessionID, forHTTPHeaderField: Constants.HTTP.playback)
        }
        return request as URLRequest
    }
    
    
}
