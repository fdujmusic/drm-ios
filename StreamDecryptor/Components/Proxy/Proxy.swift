//
//  Proxy.swift
//  RotanaDecrypt
//
//  Created by Filip Dujmušić on 17/11/2017.
//  Copyright © 2017 Ingemark. All rights reserved.
//

import Foundation
import Embassy

class Proxy: NSObject {
    
    private static var server: DefaultHTTPServer?

    private static var currentTask: StreamingDataTask?
    
    static func startServer() {
        guard
            let loop = try? SelectorEventLoop(selector: KqueueSelector())
        else {
            print("ERROR: Could not initialize event loop!")
            return
        }
        let server =
            DefaultHTTPServer(
                eventLoop: loop,
                interface: Host.local.host,
                port: Host.local.port,
                app: { (environ, startResponse, sendBody) in
                    let loop = environ["embassy.event_loop"] as! EventLoop
                    self.currentTask?.cancel()
                    self.currentTask = StreamingDataTask(
                        environment: environ,
                        startResponse: startResponse,
                        sendBody: sendBody,
                        loop: loop
                    )
                    self.currentTask?.resume()
                }
            )
        
        do {
            try server.start()
            DispatchQueue.global().async {
                loop.runForever()
            }
        } catch {
            print(error)
        }
        
        self.server = server
    }
    
}
