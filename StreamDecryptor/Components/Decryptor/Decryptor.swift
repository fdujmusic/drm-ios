//
//  Decryptor.swift
//  RotanaDecrypt
//
//  Created by Filip Dujmušić on 15/11/2017.
//  Copyright © 2017 Ingemark. All rights reserved.
//

import Foundation
import CryptoSwift

extension Data {
    func toBytes() -> [UInt8] {
        return withUnsafeBytes { (u8Ptr: UnsafePointer<UInt8>) -> [UInt8] in
            let buffer = UnsafeBufferPointer(start: u8Ptr, count: count)
            return Array<UInt8>(buffer)
        }
    }
}

class Decryptor {
    
    static var cancel: Bool = false
    
    let AES_BLOCK_SIZE = 16
    
    // Ultra mega secret shared 256bit key (also configured on AWS)
    private let commonKey: String = "a5f918fe384a670827ad999876b87a893fe6d7e6b3cb45d005a65407c27982e4"
  
    // Final key
    private var finalKey: String?
    
    // Initialization vector
    private var ivPrefix: String?
    private var ivOffset: UInt64 = 0
    
    private var contentSize: Int
    private var contentOffset: Int
    
    private var leftoverBytesBuffer: [UInt8] = []
    private var bytesToDrop = 0
    
    // MARK: - Precompute final key from file name and initialize decryptor
    init?(for url: URL, offset: Int, size: Int) {
        
        guard let ivPrefix = url.getIVPrefix() else {
            print("ERROR: Could not extract IV from url!")
            return nil
        }
        
        guard let encryptedLocalKey = url.getEncryptedLocalKey() else {
            print("ERROR: Could not extract IV from url!")
            return nil
        }
        
        let encryptedLocalKeyBytes = Array<UInt8>(hex: encryptedLocalKey)
        let decryptedLocalKey = Decrypt.fast(
            data: Data(encryptedLocalKeyBytes),
            keyHex: commonKey,
            ivHex: ivPrefix + "00000000"
        )?.toHexString() ?? ""
        
        self.finalKey = Digest.sha256((commonKey + decryptedLocalKey).bytes).toHexString()
        self.ivPrefix = ivPrefix
        self.contentSize = size
        self.contentOffset = offset
        
        bytesToDrop = offset % 16
        leftoverBytesBuffer = [UInt8](repeating: 0, count: bytesToDrop)
        ivOffset = UInt64(offset / 16)
    }
    
    func decrypt(data: Data) -> Data? {

        let totalData = leftoverBytesBuffer + data.toBytes()
        let decryptableBlocksCount = totalData.count / AES_BLOCK_SIZE
        let decryptableBytesCount = decryptableBlocksCount * AES_BLOCK_SIZE
        
        leftoverBytesBuffer = Array(totalData.suffix(totalData.count - decryptableBytesCount))
        
        let decryptableData = Array(totalData.prefix(decryptableBytesCount))
        let currentCTR = String(format: "%08x", ivOffset)
        ivOffset = ivOffset + UInt64(decryptableBytesCount / AES_BLOCK_SIZE)
        
        guard
            let finalKey = finalKey,
            let ivPrefix = ivPrefix
        else {
            print("ERROR: AES decryptor not initialized!")
            return nil
        }
        
        guard
            let decryptedData = Decrypt.fast(
                data: Data(decryptableData),
                keyHex: finalKey,
                ivHex: ivPrefix + currentCTR
            )
        else { return nil }
        if bytesToDrop > 0 {
            bytesToDrop = 0
            return decryptedData.suffix(from: bytesToDrop)
        }
        return decryptedData
    }

    func finish() -> Data? {
        let currentCTR = String(format: "%08x", ivOffset)
        let leftoverBytes = Data(leftoverBytesBuffer)
        guard
            let finalKey = finalKey,
            let ivPrefix = ivPrefix,
            let decodedBytes = Decrypt.fast(data: leftoverBytes, keyHex: finalKey, ivHex: ivPrefix + currentCTR)
        else {
                print("ERROR: AES decryptor not initialized!")
                return nil
        }
        return Data(decodedBytes)
    }
    
}

