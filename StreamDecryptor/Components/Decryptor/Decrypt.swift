//
//  AES.swift
//  StreamDecryptor
//
//  Created by Filip Dujmusic on 03/06/2018.
//  Copyright © 2018 Ingemark. All rights reserved.
//

import Foundation
import CryptoSwift

extension String {
    var hexa2Bytes: [UInt8] {
        let hexa = Array(self)
        return stride(from: 0, to: count, by: 2).flatMap { UInt8(String(hexa[$0..<$0.advanced(by: 2)]), radix: 16) }
    }
}

struct Decrypt { private init () { }

    static func fast(data: Data, keyHex: String, ivHex: String) -> Data? {
        var cryptor = UnsafeMutablePointer<CCCryptorRef?>.allocate(capacity: 1)
        CCCryptorCreateWithMode(
            CCOperation(kCCDecrypt),
            CCMode(kCCModeCTR),
            CCAlgorithm(kCCAlgorithmAES),
            CCPadding(ccNoPadding),
            ivHex.hexa2Bytes,
            keyHex.hexa2Bytes,
            kCCKeySizeAES256,
            nil,
            0,
            0,
            CCModeOptions(kCCModeOptionCTR_BE),
            cryptor
        )
        var dataOut = [UInt8](repeating: 0, count: data.count)
        CCCryptorUpdate(
            cryptor.pointee,
            data.toBytes(),
            data.count,
            &dataOut,
            data.count, nil
        )
        return Data(dataOut)
    }
    
    static func slow(data: Data, key: [UInt8], iv: [UInt8]) -> Data? {
        guard
            let aes = try? AES(
                key: key,
                blockMode: .CTR(iv: iv),
                padding: .noPadding
            )
        else { return nil }
        return try? Data(aes.decrypt(data.toBytes()))
    }
    
}
