//
//  ScreenOrchestrator.swift
//  RotanaDecrypt
//
//  Created by Filip Dujmušić on 15/11/2017.
//  Copyright © 2017 Ingemark. All rights reserved.
//

import UIKit

struct ScreenOrchestrator { private init() { }
    
    private static var navigationController: UINavigationController?
    
    static func initialize(with navigationVC: UINavigationController) {
        self.navigationController = navigationVC
    }
    
    static func presentAudioPlayer(for url: String) {
        let playerVC = PlayerViewController()
        playerVC.contentURL = url
        navigationController?.viewControllers = [playerVC]
    }
    
}
