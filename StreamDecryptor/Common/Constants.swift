//
//  Constants.swift
//  RotanaDecrypt
//
//  Created by Filip Dujmušić on 23/11/2017.
//  Copyright © 2017 Ingemark. All rights reserved.
//

import Foundation

struct Constants { private init() { }
    
    struct ENV { private init() { }
        static let method       = "REQUEST_METHOD"
        static let path         = "PATH_INFO"
        static let encoding     = "HTTP_ACCEPT_ENCODING"
        static let range        = "HTTP_RANGE"
        static let connection   = "HTTP_CONNECTION"
        static let language     = "HTTP_ACCEPT_LANGUAGE"
        static let accept       = "HTTP_ACCEPT"
        static let playback     = "HTTP_X_PLAYBACK_SESSION_ID"
    }
    
    struct HTTP { private init() { }
        static let encoding     = "Accept-Encoding"
        static let range        = "Range"
        static let connection   = "Connection"
        static let language     = "Accept-Language"
        static let accept       = "Accept"
        static let playback     = "X-Playback-Sesssion-id"
        static let length       = "Content-Length"
    }

}
