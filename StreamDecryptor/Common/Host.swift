//
//  Host.swift
//  RotanaDecrypt
//
//  Created by Filip Dujmušić on 23/11/2017.
//  Copyright © 2017 Ingemark. All rights reserved.
//

import Foundation

enum Scheme: String {
    case https = "https"
    case http = "http"
}

struct Host {
    
    static let s3 = Host(
        scheme: .https,
        host: "s3.eu-central-1.amazonaws.com",
        port: 443
    )
    
    static let local =  Host(
        scheme: .http,
        host: "localhost",
        port: 8080
    )
    
    let scheme: Scheme
    let host: String
    let port: Int
    
    var fullPath: String {
        return scheme.rawValue + "://" + host + ":" + "\(port)"
    }
    
    init(scheme: Scheme, host: String, port: Int = 80) {
        self.scheme = scheme
        self.host = host
        self.port = port
    }
    
}
