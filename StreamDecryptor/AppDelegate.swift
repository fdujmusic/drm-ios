//
//  AppDelegate.swift
//  RotanaDecrypt
//
//  Created by Filip Dujmušić on 05/11/2017.
//  Copyright © 2017 Ingemark. All rights reserved.
//

import UIKit
import Embassy
import StreamingKit
import AVFoundation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var server: DefaultHTTPServer?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        try? AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
        try? AVAudioSession.sharedInstance().setActive(true)
        
        Proxy.startServer()
        
        initializeScreen()
        ScreenOrchestrator.presentAudioPlayer(
            for: "https://s3.eu-central-1.amazonaws.com/im-drm-encrypted/vojkov-kakoto_b4897946827b32c13d14425290731fb3ae7a4134c0bae122c2ecc714df636040_71a96730b9e43a33b69d8344.mp3"
        )
        
        return true
    }
    
    func initializeScreen() {
        let navigationVC = UINavigationController()
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = navigationVC
        window?.makeKeyAndVisible()
        
        ScreenOrchestrator.initialize(with: navigationVC)
    }

}


