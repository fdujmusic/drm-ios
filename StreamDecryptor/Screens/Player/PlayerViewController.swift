//
//  ViewController.swift
//  RotanaDecrypt
//
//  Created by Filip Dujmušić on 05/11/2017.
//  Copyright © 2017 Ingemark. All rights reserved.
//

import UIKit
import AVKit
import CryptoSwift
import StreamingKit
import SnapKit

class PlayerViewController: UIViewController {

    var header: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "speaker")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    var progressBar: UISlider = {
        let slider = UISlider()
        slider.isContinuous = false
        return slider
    }()
    
    var leftTimeLabel: UILabel = {
        let label = UILabel()
        label.text = "-"
        return label
    }()
    
    var rightTimeLabel: UILabel = {
        let label = UILabel()
        label.text = "-"
        return label
    }()
    
    var player: STKAudioPlayer?
    var contentURL: String?
    
    var timer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        view.backgroundColor = .white
        title = "Stream Decryptor"
        
        addHeader()
        addSlider()
        startTimer()
        playURL()
    }
    
    private func addHeader() {
        view.addSubview(header)
        header.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(100)
            make.centerX.equalToSuperview()
            make.height.equalTo(170)
        }
    }
    
    private func addSlider() {
        view.addSubview(progressBar)
        progressBar.snp.makeConstraints { make in
            make.top.equalTo(header.snp.bottom).offset(50)
            make.left.right.equalToSuperview().inset(20)
        }
        
        progressBar.addTarget(
            self,
            action: #selector(sliderChanged(sender:)),
            for: .valueChanged
        )
        
        view.addSubview(leftTimeLabel)
        leftTimeLabel.snp.makeConstraints { make in
            make.left.equalTo(progressBar)
            make.centerY.equalTo(progressBar).offset(25)
        }
        
        view.addSubview(rightTimeLabel)
        rightTimeLabel.snp.makeConstraints { make in
            make.right.equalTo(progressBar)
            make.centerY.equalTo(progressBar).offset(25)
        }
    }
    
    private func startTimer() {
        Timer.scheduledTimer(
            timeInterval: 0.1,
            target: self,
            selector: #selector(timerFired),
            userInfo: nil,
            repeats: true
        )
    }
    
    private func playURL() {
        guard
            let urlString = contentURL,
            let url = URL(string: urlString),
            let localURL = url.changeBaseTo(.local)
        else {
            print("ERROR: Missing content URL!")
            return
        }
        player = STKAudioPlayer()
        player?.play(localURL)
    }
    
    @objc
    private func timerFired() {
        guard
            let progress = player?.progress,
            let duration = player?.duration
        else { return }
        
        leftTimeLabel.text = getMMSS(seconds: progress)
        rightTimeLabel.text = getMMSS(seconds: duration)
        
        if progressBar.isTouchInside { return }
        progressBar.minimumValue = 0
        progressBar.maximumValue = Float(duration)
        progressBar.setValue(Float(progress), animated: true)
    }
    
    @objc
    private func sliderChanged(sender: UISlider) {
        player?.seek(toTime: Double(progressBar.value))
    }
    
    func getMMSS(seconds: Double?) -> String {
        guard
            let seconds = seconds
        else { return "-" }
        let min = String(format: "%02d", Int(seconds / 60))
        let sec = String(format: "%02d", Int(seconds) % 60)
        return "\(min):\(sec)"
    }

}

